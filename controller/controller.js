'use strict';

var response = require('../callback/res');
var connection = require('../config/conn');
var crypto = require('crypto');
var multer = require('multer');

exports.getusersall = function (req, res) {
    connection.query('SELECT * FROM user', function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.getuserbyid = function (req, res) {
    connection.query('SELECT * FROM user WHERE id=?', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.deleteuserall = function (req, res) {
    connection.query('DELETE  FROM user ', function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.success(rows, res)
        }
    });
};

exports.deleteuserbyid = function (req, res) {
    connection.query('DELETE FROM user WHERE id=?', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.success(rows, res)
        }
    });
};

exports.updateuserbyid = function (req, res) {

    var user_id = req.body.user_id;
    var name = req.body.name;
    var city = req.body.city;
    var age = req.body.age;

    connection.query('UPDATE user SET name = ?, city = ?, age = ?  WHERE id = ?',
        [name, city, parseFloat(age), parseFloat(user_id)],
        function (error, rows, fields) {
            if (error) {
                console.log(error)
            } else {
                response.success("Berhasil merubah user!", res)
            }
        });
};

exports.registerakun = function (req, res) {

    var email = req.body.email;
    var no_hp = req.body.no_hp;
    var password = req.body.password;
    var hash = crypto.createHash('md5').update(password).digest('hex');

    connection.query('INSERT INTO user (email , no_hp , password) values (?,?,?)', [email, no_hp, hash],
        function (error) {
            if (error) {
                console.log(error)
            } else {
                response.success("Berhasil menambahkan user!", res)
            }
        });
};



exports.login = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var hash = crypto.createHash('md5').update(password).digest('hex');
    connection.query('SELECT * FROM user WHERE email = ?', [email], function (error, results, fields) {
        if (error) {
            res.send({
                "code": 400,
                'success': false
            })
        } else {
            if (results.length > 0) {
                if (results[0].password == hash) {
                    res.send({
                        "code": 200,
                        'success': true,
                        'user_id': results[0].id
                    });
                }
                else {
                    res.send({
                        "code": 204,
                        'success': false
                    });
                }
            }
            else {
                res.send({
                    "code": 204,
                    'success': false
                });
            }
        }
    });
};

exports.uploadstory = function (req, res) {

    var user_id = req.body.user_id;
    var category_id = req.body.category_id;
    var story = req.body.story;
    var date = req.body.date;
    var status_id = req.body.status_id;

    connection.query('INSERT INTO story (user_id, category_id , story , date ,  status_id) values (?,?,?,?,?)', [user_id, category_id, story , date , status_id],
        function (error , result) {
            if (error) {
                console.log(error)
            } else {
                res.send({
                    "status": 200,
                    'success': 'success',
                    'idstory':result.insertId
                })
            }
        });
};

exports.loadallstory = function (req, res) {
    connection.query('SELECT story.id , user.email, category.category, story.story , status.status , story.date , image_story.image FROM story INNER JOIN user ON story.user_id=user.id INNER JOIN category ON story.category_id=category.id INNER JOIN status ON story.status_id=status.id LEFT JOIN image_story ON image_story.story_id = story.id WHERE NOT story.status_id = 3 ORDER BY id DESC;', function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.searchstory = function (req, res) {
    connection.query('SELECT story.id, user.email, category.category, story.story , status.status , story.date FROM story INNER JOIN user ON story.user_id=user.id INNER JOIN category ON story.category_id=category.id INNER JOIN status ON story.status_id=status.id WHERE story LIKE  ?', '%' + [req.params.search] + '%', function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.category = function (req, res) {
    connection.query('SELECT * FROM category', function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.getstorybyid = function (req, res) {
    connection.query('SELECT story.id , user.email, category.category, story.story , status.status , story.date , image_story.image FROM story INNER JOIN user ON story.user_id=user.id INNER JOIN category ON story.category_id=category.id INNER JOIN status ON story.status_id=status.id LEFT JOIN image_story ON image_story.story_id = story.id WHERE story.user_id = ?', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.complaint = function (req, res) {

    var email = req.body.email;
    var no_hp = req.body.no_hp;
    var complaint = req.body.complaint;

    connection.query('INSERT INTO complaint (email, no_hp , complaint) values (?,?,?)', [email, no_hp, complaint],
        function (error) {
            if (error) {
                console.log(error)
            } else {
                response.success("Berhasil menambahkan story!", res)
            }
        });
};

var Storage = multer.diskStorage({
    destination: function (req, file, callback) {

        callback(null, "C:\\xampp\\htdocs\\imageuser\\");
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + "_" + file.originalname);
    }
});

var upload = multer({ storage: Storage }).array("image", 2);
exports.imageUser = function (req, res ) {
    upload(req, res, function (err) {
        if (err) {
            return res.end("Something went wrong:(");
        }
        res.end("success");
    });
};


exports.saveImageUser = function (req, res) {

    var image = req.body.image;
    var iduser = req.body.iduser;
    connection.query('UPDATE user SET image = ? WHERE id = ?', [image,parseFloat(iduser)],
        function (error) {
            if (error) {
                console.log(error)
            } else {
                response.success("Berhasil menambahkan user!", res)
            }
        });
};

exports.getImageUser = function (req, res) {
    connection.query('SELECT image FROM user WHERE id=?', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.changepassword = function (req, res) {

    var password = req.body.password;
    var iduser = req.body.iduser;
    var hash = crypto.createHash('md5').update(password).digest('hex');
    connection.query('UPDATE user SET password = ? WHERE id = ?', [hash,parseFloat(iduser)],
        function (error) {
            if (error) {
                console.log(error)
            } else {
                response.success("Berhasil menambahkan user!", res)
            }
        });
};

exports.status = function (req, res) {
    connection.query('SELECT * FROM status', function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};


exports.filterstory = function (req, res) {
    connection.query('SELECT story.id, user.email, category.category, story.story , status.status , story.date FROM story INNER JOIN user ON story.user_id=user.id INNER JOIN category ON story.category_id=category.id INNER JOIN status ON story.status_id=status.id WHERE NOT status_id= 3 AND story.category_id  =?', [req.params.category], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.uploadimagelowongan = function (req, res) {

    var id_userpunyagawai = req.body.id_userpunyagawai;
    var image = req.body.image;
    var lowonganupload_id = req.body.lowonganupload_id;

    connection.query('INSERT INTO image_lowongan (id_userpunyagawai, image , lowonganupload_id ) values (?,?,?)', [id_userpunyagawai, image, lowonganupload_id],
        function (error) {
            if (error) {
                console.log(error)
            } else {
                res.send({
                    "status": 200,
                    'success': 'success'
                })
            }
        });
};

exports.createaccountservice = function (req, res) {

    var nama_jasa = req.body.nama_jasa;
    var no_hp_jasa = req.body.no_hp_jasa;
    var alamat = req.body.alamat;
    var user_id = req.body.user_id;
    var image = req.body.image;

    connection.query('INSERT INTO user_punyagawai (nama_jasa, no_hp_jasa , alamat , user_id , image ) values (?,?,?,?,?)', [nama_jasa, no_hp_jasa , alamat , user_id , image],
        function (error) {
            if (error) {
                console.log(error)
            } else {
                res.send({
                    "status": 200,
                    'success': 'success'
                })
            }
        });
};

exports.getacountuserservice = function (req, res) {
    connection.query('SELECT * FROM user_punyagawai WHERE user_id=?', [req.params.user_id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.uploadlowongan = function (req, res) {

    var category_id = req.body.category_id;
    var status_id = req.body.status_id;
    var deskripsi_lowongan = req.body.deskripsi_lowongan;
    var bayaran = req.body.bayaran;
    var id_userpunyagawai = req.body.id_userpunyagawai;
    var date = req.body.date;

    connection.query('INSERT INTO lowongan_upload (category_id, status_id , deskripsi_lowongan , bayaran , id_userpunyagawai , date ) values (?,?,?,?,?,?)', [category_id, status_id , deskripsi_lowongan , bayaran , id_userpunyagawai , date],
            function (error , result) {
                if (error) {
                    console.log(error)
                } else {
                    res.send({
                        "status": 200,
                        'success': 'success',
                        'idlowongan':result.insertId
                    })
                }
        });
};

exports.getalllowongan = function (req, res) {
    connection.query('SELECT lowongan_upload.id , lowongan_upload.deskripsi_lowongan , lowongan_upload.bayaran ,user_punyagawai.image, user_punyagawai.no_hp_jasa, user_punyagawai.alamat , user_punyagawai.nama_jasa, image_lowongan.image_lowongan , category.category , status.status , lowongan_upload.date FROM lowongan_upload INNER JOIN category ON lowongan_upload.category_id = category.id INNER JOIN status on lowongan_upload.status_id = status.id LEFT JOIN image_lowongan on image_lowongan.lowonganupload_id = lowongan_upload.id INNER JOIN user_punyagawai ON lowongan_upload.id_userpunyagawai = user_punyagawai.id WHERE NOT lowongan_upload.status_id = 3 ORDER BY id DESC',function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.getalllowonganbyid = function (req, res) {
    connection.query('SELECT lowongan_upload.id , lowongan_upload.deskripsi_lowongan , lowongan_upload.bayaran ,user_punyagawai.image, user_punyagawai.no_hp_jasa, user_punyagawai.alamat , user_punyagawai.nama_jasa, image_lowongan.image_lowongan , category.category , status.status , lowongan_upload.date FROM lowongan_upload INNER JOIN category ON lowongan_upload.category_id = category.id INNER JOIN status on lowongan_upload.status_id = status.id LEFT JOIN image_lowongan on image_lowongan.lowonganupload_id = lowongan_upload.id INNER JOIN user_punyagawai ON lowongan_upload.id_userpunyagawai = user_punyagawai.id WHERE lowongan_upload.id_userpunyagawai = ? ORDER BY id DESC', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.getuserpunyagawaibyid = function (req, res) {
    connection.query('SELECT * FROM user_punyagawai WHERE user_id=?', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};

exports.detailgawaibyid = function (req, res) {
    connection.query('SELECT lowongan_upload.id , lowongan_upload.deskripsi_lowongan , lowongan_upload.bayaran ,user_punyagawai.image, user_punyagawai.no_hp_jasa, user_punyagawai.alamat , user_punyagawai.nama_jasa, image_lowongan.image_lowongan , category.category , status.status , lowongan_upload.date FROM lowongan_upload INNER JOIN category ON lowongan_upload.category_id = category.id INNER JOIN status on lowongan_upload.status_id = status.id LEFT JOIN image_lowongan on image_lowongan.lowonganupload_id = lowongan_upload.id INNER JOIN user_punyagawai ON lowongan_upload.id_userpunyagawai = user_punyagawai.id WHERE lowongan_upload.id = ?', [req.params.id], function (error, rows) {
        if (error) {
            console.log(error)
        } else {
            response.ok(rows, res)
        }
    });
};
